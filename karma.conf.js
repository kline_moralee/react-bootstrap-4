var path = require('path');
var webpack = require('webpack');

module.exports = function (config) {

  var isCI = process.env.CONTINUOUS_INTEGRATION === 'true' || process.env.CI === 'True';
  var runCoverage = process.env.COVERAGE === 'true' || isCI;

  var coverageLoaders = [];
  var coverageReporters = [];

  if (runCoverage) {
    coverageLoaders.push({
      test: /\.js$/,
      include: path.resolve('src/'),
      exclude: /__tests__/,
      loader: 'isparta'
    });

    coverageReporters.push('coverage');

    if (isCI) {
      coverageReporters.push('coveralls');
    }
  }

  config.set({
    basePath: '',

    files: ['karma.entry.js'],

    frameworks: ['jasmine'],

    browsers: ['Chrome'],

    reporters: ['spec'].concat(coverageReporters),

    preprocessors: {
      'karma.entry.js': ['webpack', 'sourcemap']
    },

    webpack: {
      devtool: 'inline-source-map',
      module: {
        loaders: [
          { test: /\.js$/, exclude: /node_modules/, loader: 'babel' }
        ].concat(coverageLoaders)
      },
      plugins: [
        new webpack.DefinePlugin({
          'process.env.NODE_ENV': JSON.stringify('test')
        })
      ]
    },

    webpackServer: {
      noInfo: true
    },

    coverageReporter: {
      reporters: [
        { type: 'html', subdir: 'html' },
        { type: 'lcovonly', subdir: '.' }
      ]
    }
  });
};
