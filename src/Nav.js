import React, { PropTypes } from 'react';
import cx from 'classnames';

const Nav = React.createClass({
  propTypes: {
    stacked: PropTypes.bool,
    variant: PropTypes.string
  },

  getDefaultProps () {
    return {
      component: 'ul'
    };
  },

  getClassNames () {
    let { className, variant, stacked } = this.props;

    return cx('nav', this.props.className, {
      'nav-stacked': !!stacked,
      [`nav-${variant}`]: !!variant,
    });
  },

  render () {
    return React.createElement(
      this.props.component,
      Object.assign({}, this.props, {
        className: this.getClassNames()
      })
    );
  }
});

export default Nav;
