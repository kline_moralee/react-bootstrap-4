import React, { PropTypes } from 'react';

const FormGroup = React.createClass({
  getDefaultProps () {
    return {
      component: 'div',
      className: 'form-group'
    };
  },

  render () {
    return React.createElement(
      this.props.component,
      this.props
    );
  }
});

export default FormGroup;
