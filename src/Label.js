import React, { PropTypes } from 'react';

const Label = React.createClass({
  getDefaultProps () {
    return {
      component: 'span',
      className: 'label'
    };
  },

  render () {
    return React.createElement(
      this.props.component,
      this.props
    );
  }
});

export default Label;
