import React, { PropTypes } from 'react';
import cx from 'classnames';

const ButtonGroup = React.createClass({
  propTypes: {
    component: PropTypes.node
  },

  getDefaultProps () {
    return {
      component: 'div',
      role: 'group'
    };
  },

  getClassNames () {
    let { className } = this.props;

    return cx('btn-group', className);
  },

  render () {
    return React.createElement(
      this.props.component,
      Object.assign({}, this.props, {
        className: this.getClassNames()
      })
    );
  }
});

export default ButtonGroup;
