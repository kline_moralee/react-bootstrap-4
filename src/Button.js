import React, { PropTypes } from 'react';
import cx from 'classnames';

const Button = React.createClass({
  propTypes: {
    type: PropTypes.oneOf(['button', 'submit', 'reset']),
    variant: PropTypes.string,
    outline: PropTypes.bool,
    size: PropTypes.string
  },

  getDefaultProps () {
    return {
      component: 'button',
      type: 'button',
      outline: false,
      variant: null,
      size: null
    };
  },

  getSizeClass () {
    if (/(small|sm)/.test(this.props.size)) {
      return 'btn-sm';
    } else if (/(large|lg)/.test(this.props.size)) {
      return 'btn-lg';
    } else {
      return null;
    }
  },

  getClassNames () {
    let { className, outline, variant } = this.props;

    return cx('btn', className, this.getSizeClass(), {
      [`btn-${variant}`]: !!variant && !outline,
      [`btn-${variant}-outline`]: !!variant && !!outline
    });
  },

  render () {
    return React.createElement(
      this.props.component,
      Object.assign({}, this.props, {
        className: this.getClassNames()
      })
    );
  }
});

export default Button;
