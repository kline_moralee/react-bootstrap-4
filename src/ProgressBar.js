import React, { PropTypes } from 'react';

const ProgressBar = React.createClass({
  propTypes: {
    max: PropTypes.number,
    value: PropTypes.number
  },

  getDefaultProps () {
    return {
      className: 'progress',
      max: 100,
      value: 0
    };
  },

  getValuePercentage () {
    let max = parseInt(this.props.max);
    let value = parseInt(this.props.value);

    if (!isNaN(max) && !isNaN(value)) {
      return `${Math.floor((value / 100) * 100)}%`;
    }
  },

  render () {
    return React.createElement(
      'progress',
      this.props,
      this.getValuePercentage()
    );
  }
});

export default ProgressBar;
