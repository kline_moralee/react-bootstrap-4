import React, { PropTypes } from 'react';
import cx from 'classnames';

const ButtonToolbar = React.createClass({
  propTypes: {
    component: PropTypes.node
  },

  getDefaultProps () {
    return {
      component: 'div',
      role: 'toolbar'
    };
  },

  getClassNames () {
    let { className } = this.props;

    return cx('btn-toolbar', className);
  },

  render () {
    return React.createElement(
      this.props.component,
      Object.assign({}, this.props, {
        className: this.getClassNames()
      })
    );
  }
});

export default ButtonToolbar;
