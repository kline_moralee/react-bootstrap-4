import React, { Component } from 'react';
import { NavBar } from 'react-bootstrap-4';

import Buttons from 'components/buttons';
import ButtonGroups from 'components/button-groups';

export default class App extends Component {
  render () {
    return (
      <div>
        <NavBar>
          <div className="container">
            Navbar goes here
          </div>
        </NavBar>

        <div className="container m-t-lg">
          <div className="row">
            <main className="col-sm-9">
              <Buttons />
              <ButtonGroups />
            </main>
            <aside className="col-sm-3">
              Navigation goes here
            </aside>
          </div>
        </div>
      </div>
    );
  }
}
