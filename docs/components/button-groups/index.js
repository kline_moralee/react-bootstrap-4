import React, { Component } from 'react';
import snippets from './snippets';
import {
  Button,
  ButtonGroup,
  ButtonToolbar
} from 'react-bootstrap-4';
import {
  DocSection,
  DocSubsection,
  ExampleCard
} from '../common';

export default class ButtonGroups extends Component {
  renderBasic () {
    return (
      <ButtonGroup>
        <Button variant="secondary">Left</Button>
        <Button variant="secondary">Middle</Button>
        <Button variant="secondary">Right</Button>
      </ButtonGroup>
    );
  }

  renderToolbar () {
    return (
      <ButtonToolbar>
        <ButtonGroup>
          <Button variant="secondary">1</Button>
          <Button variant="secondary">2</Button>
          <Button variant="secondary">3</Button>
          <Button variant="secondary">4</Button>
        </ButtonGroup>
        <ButtonGroup>
          <Button variant="secondary">5</Button>
          <Button variant="secondary">6</Button>
          <Button variant="secondary">7</Button>
        </ButtonGroup>
        <ButtonGroup>
          <Button variant="secondary">8</Button>
        </ButtonGroup>
      </ButtonToolbar>
    );
  }

  render () {
    return (
      <DocSection title="Button Group">
        <DocSubsection title="Basic example">
          <ExampleCard snippet={snippets.basic}>
            {this.renderBasic()}
          </ExampleCard>
        </DocSubsection>
        <DocSubsection title="Button toolbar">
          <ExampleCard snippet={snippets.toolbar}>
            {this.renderToolbar()}
          </ExampleCard>
        </DocSubsection>
        <DocSubsection title="Sizing">
          <ExampleCard>
            <ButtonGroup>
                ASd
            </ButtonGroup>
          </ExampleCard>
        </DocSubsection>
        <DocSubsection title="Vertical variation">
          <ExampleCard>
            <ButtonGroup>
                ASd
            </ButtonGroup>
          </ExampleCard>
        </DocSubsection>
      </DocSection>
    );
  }
}
