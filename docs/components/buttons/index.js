import React, { Component } from 'react';
import snippets from './snippets';
import {
  Button
} from 'react-bootstrap-4';
import {
  DocSection,
  DocSubsection,
  ExampleCard
} from '../common';

const variants = ['Primary', 'Secondary', 'Success', 'Warning', 'Danger'];

export default class Buttons extends Component {

  renderVariants (props = {}) {
    return variants.map((variant, i) => (
      <Button
        {...props}
        key={i}
        variant={variant.toLowerCase()}>
        {variant}
      </Button>
    ));
  }

  renderSizes (size, props = {}) {
    return ['primary', 'secondary'].map((variant, i) => (
      <Button
        {...props}
        key={i}
        variant={variant}
        size={size.toLowerCase()}>
        {`${size} button`}
      </Button>
    ));
  }

  render () {
    return (
      <DocSection title="Buttons">
        <DocSubsection id="buttons_example" title="Examples">
          <ExampleCard snippet={snippets.variants}>
            {this.renderVariants()}
          </ExampleCard>
        </DocSubsection>
        <DocSubsection id="buttons_outlined" title="Outline Buttons">
          <ExampleCard snippet={snippets.outlined}>
            {this.renderVariants({ outline: true })}
          </ExampleCard>
        </DocSubsection>
        <DocSubsection id="buttons_sizes" title="Sizes">
          <ExampleCard snippet={snippets.large}>
            {this.renderSizes('Large')}
          </ExampleCard>
          <ExampleCard snippet={snippets.small}>
            {this.renderSizes('Small')}
          </ExampleCard>
        </DocSubsection>
      </DocSection>
    );
  }
}
