export default {
  variants: require('./variants'),
  outlined: require('./outlined'),
  small: require('./small'),
  large: require('./large')
};
