import CodeMirror from 'codemirror';
import 'codemirror/addon/runmode/runmode';
import 'codemirror/mode/htmlmixed/htmlmixed';

import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import cx from 'classnames';

export default class CodeSnippet extends Component {
  static propTypes = {
    snippet: PropTypes.string
  }

  componentDidMount () {
    CodeMirror.runMode(
      this.props.snippet,
      'htmlmixed',
      ReactDOM.findDOMNode(this.refs.code)
    );
  }

  render () {
    let classes = cx('CodeMirror', this.props.className);

    return (
      <pre className={classes}>
        <code ref="code">
          {this.props.snippet}
        </code>
      </pre>
    );
  }
}
