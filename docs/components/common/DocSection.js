import React, { Component, PropTypes } from 'react';

export default class DocSection extends Component {
  static propTypes = {
    title: PropTypes.string
  }

  static defaultProps = {
    title: 'Section Title'
  }

  renderTitle () {
    return (
      <h2 className="doc-section-title">
        {this.props.title}
      </h2>
    );
  }

  render () {
    return (
      <section className="doc-section">
        {this.renderTitle()}
        {this.props.children}
      </section>
    );
  }
}
