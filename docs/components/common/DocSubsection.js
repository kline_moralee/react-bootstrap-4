import React, { Component, PropTypes } from 'react';

export default class DocSubsection extends Component {
  static propTypes = {
    title: PropTypes.string
  }

  static defaultProps = {
    title: 'Subsection Title'
  }

  renderTitle () {
    return (
      <h4 className="doc-subsection-title">
        {this.props.title}
      </h4>
    );
  }

  render () {
    return (
      <div className="doc-subsection">
        {this.renderTitle()}
        {this.props.children}
      </div>
    );
  }
}
