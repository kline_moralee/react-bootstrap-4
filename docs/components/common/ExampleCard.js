import React, { Component, PropTypes } from 'react';
import CodeSnippet from './CodeSnippet';

export default class ExampleCard extends Component {
  static propTypes = {
    snippet: PropTypes.string,
  }

  renderSnippet () {
    if (this.props.snippet) {
      return (
        <CodeSnippet snippet={this.props.snippet} />
      );
    }
  }

  render () {
    return (
      <div className="card card-example">
        <div className="card-block">
          {this.props.children}
        </div>
        {this.renderSnippet()}
      </div>
    );
  }
}
