import React from 'react';
import * as TestUtils from './TestUtils';
import ButtonGroup from '../src/ButtonGroup';

describe('<ButtonGroup />', function () {
  it('should render', () => {
    let result = TestUtils.shallowRender(ButtonGroup);

    expect(result).toBeDefined();

    expect(result.type).toBe('div',
      'should render as a `div` element by default');
    expect(result.props.className).toBe('btn-group',
      'should have `btn-group` class by default');
    expect(result.props.role).toBe('group',
      'should have `group` role by default');
  });

  it('should change `component`', () => {
    let result = TestUtils.shallowRender(ButtonGroup, {
      component: 'span'
    });

    expect(result.type).toBe('span');
  });
});
