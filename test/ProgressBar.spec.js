import React from 'react';
import * as TestUtils from './TestUtils';
import ProgressBar from '../src/ProgressBar';

describe('<ProgressBar />', function () {
  it('should render', () => {
    let result = TestUtils.shallowRender(ProgressBar);

    expect(result).toBeDefined();

    expect(result.type).toBe('progress',
      'should render as a `progress` element');
    expect(result.props.className).toBe('progress',
      'should have `progress` class by default');
    expect(result.props.value).toBe(0,
      'should have a `value` of 0 by default');
    expect(result.props.max).toBe(100,
      'should have a `max` of 100 by default');
  });

  it('should render the value as a percentage', () => {
    let result = TestUtils.shallowRender(ProgressBar, {
      max: 100,
      value: 43
    });

    expect(result.props.children).toBe('43%');
  });

  it('should ensure `value` and `max` are valid numbers', () => {
    let result = TestUtils.shallowRender(ProgressBar, {
      max: 'foo'
    });

    expect(result.props.children).not.toBeDefined();

    result = TestUtils.shallowRender(ProgressBar, {
      value: 'foo'
    });

    expect(result.props.children).not.toBeDefined();
  });
});
