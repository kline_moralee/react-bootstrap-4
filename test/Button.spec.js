import * as TestUtils from './TestUtils';
import Button from '../src/Button';

describe('<Button />', function () {
  it('should render', () => {
    let result = TestUtils.shallowRender(Button);

    expect(result).toBeDefined();

    expect(result.type).toBe('button',
      'should render as `button` element by default');
    expect(result.props.type).toBe('button',
      'should be type `button` by default');
    expect(result.props.className).toBe('btn',
      'should have `btn` class by default');
  });

  it('should change `component`', () => {
    let result = TestUtils.shallowRender(Button, { component: 'a' });

    expect(result.type).toBe('a');
  });

  it('should add the `variant` class', () => {
    let result = TestUtils.shallowRender(Button, {
      variant: 'primary'
    });

    expect(result.props.className).toEqual(jasmine.stringMatching(/btn-primary/));
  });

  it('should add the `outline` class', () => {
    let result = TestUtils.shallowRender(Button, {
      outline: true
    });

    expect(result.props.className).not.toEqual(jasmine.stringMatching(/-outline/));

    result = TestUtils.shallowRender(Button, {
      outline: true,
      variant: 'primary'
    });

    expect(result.props.className).toEqual(jasmine.stringMatching(/primary-outline/));
  });

  it('should add the `size` class', () => {
    let result = TestUtils.shallowRender(Button, {
      size: 'small'
    });

    expect(result.props.className).toEqual(jasmine.stringMatching(/btn-sm/));

    result = TestUtils.shallowRender(Button, {
      size: 'lg'
    });

    expect(result.props.className).toEqual(jasmine.stringMatching(/btn-lg/));
  });
});
