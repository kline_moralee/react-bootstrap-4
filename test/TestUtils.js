import React from 'react';
import TestUtils from 'react-addons-test-utils';

export const createShallowRenderer = (Component, props = {}) => {
  let shallowRenderer = TestUtils.createRenderer();
  shallowRenderer.render(<Component {...props} />);

  return () => shallowRenderer.getRenderOutput();
};

export const shallowRender = (Component, props = {}) => {
  let shallowRenderer = createShallowRenderer(Component, props);

  return shallowRenderer();
};
