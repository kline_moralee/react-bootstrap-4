import React from 'react';
import * as TestUtils from './TestUtils';
import FormGroup from '../src/FormGroup';

describe('<FormGroup />', function () {
  it('should render', () => {
    let result = TestUtils.shallowRender(FormGroup);

    expect(result).toBeDefined();
    expect(result.type).toBe('div',
      'should render as `div` element by default');
    expect(result.props.className).toBe('form-group',
      'should have `form-group` class by default');
  });

  it('should change `component`', () => {
    let result = TestUtils.shallowRender(FormGroup, { component: 'fieldset' });

    expect(result.type).toBe('fieldset');
  });
});
