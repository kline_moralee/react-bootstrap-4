import React from 'react';
import * as TestUtils from './TestUtils';
import Nav from '../src/Nav';

describe('<Nav />', function () {
  it('should render', () => {
    let result = TestUtils.shallowRender(Nav);

    expect(result).toBeDefined();
    expect(result.type).toBe('ul',
      'should render as a `ul` element by default');
    expect(result.props.className).toBe('nav',
      'should have `nav` class by default');
  });

  it('should change `component`', () => {
    let result = TestUtils.shallowRender(Nav, {
      component: 'div'
    });

    expect(result.type).toBe('div');
  });

  it('should add the `variant` class', () => {
    let result = TestUtils.shallowRender(Nav, {
      variant: 'pills'
    });

    expect(result.props.className).toEqual(jasmine.stringMatching(/nav-pills/));
  });

  it('should add the `stacked` class', () => {
    let result = TestUtils.shallowRender(Nav, {
      stacked: true
    });

    expect(result.props.className).toEqual(jasmine.stringMatching(/nav-stacked/));
  });
});
