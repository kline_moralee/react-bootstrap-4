import * as TestUtils from './TestUtils';
import Label from '../src/Label';


describe('<Label />', function () {
  it('should render', () => {
    let result = TestUtils.shallowRender(Label);

    expect(result).toBeDefined();

    expect(result.type).toBe('span',
      'should render as a `span` element by default');
    expect(result.props.className).toBe('label',
      'should have `label` class by default');
  });

  it('should change `component`', () => {
    let result = TestUtils.shallowRender(Label, { component: 'i' });

    expect(result.type).toBe('i');
  });
});
