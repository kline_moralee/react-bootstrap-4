import React from 'react';
import * as TestUtils from './TestUtils';
import ButtonToolbar from '../src/ButtonToolbar';

describe('<ButtonToolbar />', function () {
  it('should render', () => {
    let result = TestUtils.shallowRender(ButtonToolbar);

    expect(result).toBeDefined();

    expect(result.type).toBe('div',
      'should render as a `div` element by default');
    expect(result.props.className).toBe('btn-toolbar',
      'should have `btn-toolbar` class by default');
    expect(result.props.role).toBe('toolbar',
      'should have `toolbar` role by default');
  });

  it('should change `component`', () => {
    let result = TestUtils.shallowRender(ButtonToolbar, {
      component: 'span'
    });

    expect(result.type).toBe('span');
  });
});
