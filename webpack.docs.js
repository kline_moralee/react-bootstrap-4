const path = require('path');
const webpack = require('webpack');

module.exports = {
  context: __dirname,

  entry: [
    'webpack/hot/dev-server',
    'webpack-dev-server/client?http://localhost:3000',
    './docs/index.js'
  ],

  output: {
    filename: 'bundle.js',
    path: __dirname + '/__build__',
    publicPath: '/__build__/'
  },

  resolve: {
    root: path.join(__dirname, 'node_modules'),
    modulesDirectories: ['node_modules', 'docs'],
    extensions: ['', '.js', '.code.jsx'],
    alias: {
      'react-bootstrap-4': path.join(__dirname, 'src')
    }
  },

  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel?babelrc=.dev.babelrc'
    }, {
      test: /\.code\.jsx$/,
      loader: 'raw'
    }, {
      test: /\.scss$/,
      loader: 'style!css!sass!autoprefixer?browsers=last 2 versions'
    }]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
    })
  ]
};
